const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  db: {
    url: 'mongodb://localhost:27017',
    name: 'task'
  }
};

