const express = require('express');
const Task = require('../models/Task');
const User = require('../models/User');

const createRouter = () => {
  const router = express.Router();

  const myMiddleware = (req, res, next) => {
    const token = req.get('Token');
    if (!token) {
      return res.status(401).send({error: 'Token not provided!'});
    }
    User.findOne({token: token}).then(user => {
      if (!user) {
        return res.sendStatus(401); // Send error, no such user!
      }
      req.user = user;
      next();
    });
  };

  router.get('/', myMiddleware, (req, res) => {
    Task.findOne({user: req.user._id})
      .then(results => {
        res.send(results)
      })
      .catch(() => res.sendStatus(500));
  });

  router.post('/', (req, res) => {
    const task = new Task(req.body);

    task.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });
  router.get('/:id', (req, res) => {
    Task.findById(req.params.id)
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404).send({error:'Not found'});
      })
      .catch(() => res.sendStatus(500));
  });

  router.put('/:id', (req, res) => {
    const editTask = Task.findByIdAndUpdate(req.params.id)
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404).send({error:'Not updated'});
      })
      .catch(() => res.sendStatus(500));
  });

  router.delete('/:id', (req, res) => {
    Task.findByIdAndRemove(req.params.id, (err, todo) => {
      if (err) return res.status(500).send(err);
      const response = {
        message: "Task successfully deleted",
        id: todo._id
      };
      return res.status(200).send(response);
    });
  });


  return router;
};

module.exports = createRouter;

